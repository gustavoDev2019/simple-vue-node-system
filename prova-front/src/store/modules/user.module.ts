import { getLocalUser } from '@/services/authServices';

const user = getLocalUser();

export default {
   namespaced: true,
   state: { user },
   getters: {
      user(state: any) {
        return state.user;
      },
   },
   mutations: {
      loginSuccess(state: any, payload: any) {
        state.user = Object.assign({}, payload.user, {token: payload.token});
        localStorage.setItem('user', JSON.stringify(state.user));
      },
      logout(state: any) {
         localStorage.removeItem('user');
         state.user = null;
      },
   },
   actions: {
      login(context: any) {
         context.commit('login');
      },
   },
};
