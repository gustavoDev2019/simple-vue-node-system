export function initialize(store: any, router: any) {
  router.beforeEach((to: any, from: any, next: any) => {
    const requiresAuth = to.matched.some( (record: any) => record.meta.requiresAuth);
    const currentUser = store.getters['user/user'];

    if (requiresAuth && !currentUser) {
      next('/login');
    }
    next();
  });
}
