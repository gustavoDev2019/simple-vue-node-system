import axios from 'axios';

const host = 'http://127.0.0.1:3000/';

export function signUp(data: any): Promise<any> {
  const url = host + 'user/';
  const headers = {
    'X-Requested-With': 'XMLHttpRequest',
    'Content-Type': 'application/json;charset=UTF-8',
  };

  return new Promise( (res, rej) => {
    axios.post(url, data, { headers }).then( (response: any ) => {
      res(response.data);
    }).catch( (err: any) => {
      rej(err);
    });
  });
}

export function login(data: any): Promise<any> {
  const url = host + 'user/authenticate/';
  const headers = { 'X-Requested-With': 'XMLHttpRequest',
                    'Content-Type': 'application/json;charset=UTF-8' };

  return new Promise( (res, rej) => {
    axios.post(url, data, { headers })
    .then( (response: any ) => {
      res(response.data);
    }).catch( (err: any) => {
      rej(err);
    });
  });
}

export function getLocalUser() {
  const userString = localStorage.getItem('user');
  if (!userString) {
    return null;
  }
  return JSON.parse(userString);
}
