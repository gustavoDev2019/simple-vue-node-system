import { Employee } from '../models/employee';
import axios, { Method } from 'axios';

const host = 'http://127.0.0.1:3000/';

export function listUser(token: string): Promise<any> {
  const url = host + 'user/';
  const headers = {
    'Authorization': token,
    'X-Requested-With': 'XMLHttpRequest',
    'Content-Type': 'application/json;charset=UTF-8',
  };
  return new Promise((res, rej) => {
    axios.get( url, { headers } )
    .then((response) => { res(response.data); })
    .catch((err) => {
      if (err.response && err.response.data) {
        err = err.response.data;
      }
      rej(err);
    });
  });
}

export function listEmployee(id: string, token: string): Promise<any> {
  const url = host + 'employee/list/' + id;
  const headers = {
    'Authorization': token,
    'X-Requested-With': 'XMLHttpRequest',
    'Content-Type': 'application/json;charset=UTF-8',
  };
  return new Promise((res, rej) => {
    axios.get( url, { headers } )
    .then((response) => { res(response.data); })
    .catch((err) => {
      if (err.response && err.response.data) {
        err = err.response.data;
      }
      rej(err);
    });
  });
}

export function getEmployee(id: string, token: string): Promise<any> {
  const url = host + 'employee/' + id;
  const headers = {
    'Authorization': token,
    'X-Requested-With': 'XMLHttpRequest',
    'Content-Type': 'application/json;charset=UTF-8',
  };
  return new Promise((res, rej) => {
    axios.get( url, { headers } )
    .then((response) => { res(response.data); })
    .catch((err) => {
      if (err.response && err.response.data) {
        err = err.response.data;
      }
      rej(err);
    });
  });
}

export function createEmployee(data: any, token: string): Promise<any> {
  const url = host + 'employee/';
  const headers = {
    'Authorization': token,
    'X-Requested-With': 'XMLHttpRequest',
    'Content-Type': 'application/json;charset=UTF-8',
  };
  return new Promise((res, rej) => {
    axios.post( url, data, { headers } )
    .then((response) => { res(response.data); })
    .catch((err) => {
      if (err.response && err.response.data) {
        err = err.response.data;
      }
      rej(err);
    });
  });
}

export function updateEmployee(id: string, data: any, token: string): Promise<any> {
  const url = host + 'employee/' + id;
  const headers = {
    'Authorization': token,
    'X-Requested-With': 'XMLHttpRequest',
    'Content-Type': 'application/json;charset=UTF-8',
  };
  return new Promise((res, rej) => {
    axios.put( url, data, { headers } )
    .then((response) => { res(response.data); })
    .catch((err) => {
      if (err.response && err.response.data) {
        err = err.response.data;
      }
      rej(err);
    });
  });
}

export function deleteEmployee(data: any, token: string): Promise<any> {
  const url = host + 'employee/';
  const headers = {
    'Authorization': token,
    'X-Requested-With': 'XMLHttpRequest',
    'Content-Type': 'application/json;charset=UTF-8',
  };
  return new Promise((res, rej) => {
    axios.delete( url, { data, headers } )
    .then((response) => { res(response.data); })
    .catch((err) => {
      if (err.response && err.response.data) {
        err = err.response.data;
      }
      rej(err);
    });
  });
}
