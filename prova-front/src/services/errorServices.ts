import swal from 'sweetalert2';

export function handleError(error: any): void {
  let message = '';
  if (error && error.length > 0) {
    error.forEach( (e: any) => {
      message = message.concat('.\n' + e.message);
    });
  }

  swal.fire({
    icon: 'error',
    title: 'Erro ao processar requisão...',
    text: message,
  });
}
