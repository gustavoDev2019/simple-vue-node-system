export class Employee {
  public _id?: string;
  public user?: string;
  public name?: string;
  public email?: string;
  public doc?: string;
  public rank?: number;
}
