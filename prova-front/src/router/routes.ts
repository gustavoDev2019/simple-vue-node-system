const Sign = () => import('@/views/Sign.vue');
const Login = () => import('@/views/Login.vue');
const EmployeeList = () => import('@/views/ListEmployee.vue');
const Employee = () => import('@/views/Employee.vue');

const routes = [
  {
    path: '/',
    name: 'home',
    redirect: '/login',
  },
  {
    path: '/sign-up',
    name: 'sign-up',
    component: Sign,
  },
  {
    path: '/login',
    name: 'login',
    component: Login,
  },
  {
    path: '/list',
    name: 'list',
    component: EmployeeList,
    meta: {
      requiresAuth: true,
    },
  },
  {
    path: '/employee/:id',
    name: 'edit-employee',
    component: Employee,
    meta: {
      requiresAuth: true,
    },
  },
  {
    path: '/employee',
    name: 'create-employee',
    component: Employee,
    meta: {
      requiresAuth: true,
    },
  },
];

export default routes;
