import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import { initialize } from './services/general';

Vue.config.productionTip = false;
initialize(store, router);

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount('#app');
