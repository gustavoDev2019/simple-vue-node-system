'use strict';

const mongoose = require('mongoose');
const User = mongoose.model('User');

exports.create = async (data) => {
  let user = new User(data);
  await user.save();
}

exports.get = async () => {  
  let data = await User.find({}, 'name email password');
  return data;
}

exports.getById = async (id) => {
  let data = await User.findOne({_id: id}, 'name email password');
  return data;
}

exports.update = async (id, data) => {
  await User.findByIdAndUpdate(id, 
              {
                $set: {
                        name: data.name,
                        email: data.email,
                        password: data.password,
                      }
              });
}

exports.delete = async (id) => {
  await User.findByIdAndRemove(id)
}