'use strict';

const Validation = require('../validators/validator');
const mongoose = require('mongoose');
const Employee = mongoose.model('Employee');

exports.get = (user) => {
  return Employee.find({user: user}, 'name email doc rank user');
  /*return Employee.find({}, 'name doc rank')
                  .populate('user', 'name email password');*/
}

exports.getById = async (id) => {
  let data = await Employee.findOne({_id: id}, 'name email doc rank tags');
  return data;
}

exports.getByDoc = async (doc) => {
  let data = await Employee.findOne({ doc: doc });
  if(data) return data;
  return false;
}

exports.getByEmail = async (email) => {
  let data = await Employee.findOne({ email: email });
  if(data) return data;
  return false;
}

exports.validateForm = (body) => {  
  let validation = new Validation();
  validation.hasMinLen(body.name, 3, "O nome deve conter mais de 3 caracteres.");
  validation.isFixedLen(body.doc, 12, "O documento deve conter 12 caracteres.");
  if(!validation.isValid()){
    return validation.errors()
  }
}

exports.create = (data) => {
  let employee = new Employee(data);
  return employee.save();
}
