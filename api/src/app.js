'use strict'

const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const config = require('./config');

const app = express();

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
  res.header('Access-Control-Allow-Methods', 'PUT, POST, GET, DELETE, OPTIONS');
  next();
});

// Connecta ao banco
mongoose.connect(
  config.connectionString, 
  { useNewUrlParser: true, useUnifiedTopology: true });

// Carrega os Models
const Employee = require('./models/employee');
const User = require('./models/user');

//Carrega as rotas
const indexRoute = require('./routes/index-route');
const employeeRoute = require('./routes/employee-route');
const userRoute = require('./routes/user-route');


//Middleware for processing the requisition body
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use('/', indexRoute);
app.use('/employee', employeeRoute);
app.use('/user', userRoute);

module.exports = app;