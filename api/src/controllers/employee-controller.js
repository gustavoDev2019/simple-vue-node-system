'use strict';

const mongoose = require('mongoose');
const Employee = mongoose.model('Employee');
const repository = require('../repositories/employee-repository');

exports.get = (req, res, next) => {
  let user = req.params.user;
  repository.get(user)
  .then(data => {
    res.status(200).send(data);
  })
  .catch( e => {
    res.status(400).send(e);
  });
}; 

exports.getById = async (req, res, next) => {
  let id = req.params.id;
  try {
    let data = await repository.getById(id);
    res.status(201).send(data);
  } catch (e) {
    res.status(500).send({ 
      message: "Falha ao processar requisição!"
    });
  }
}; 

exports.post = async (req, res, next) => {
  const errors = repository.validateForm(req.body)
  if (errors) {
    res.status(400).send(validation.errors()).end();
    return;
  }

  try {
    if ( await repository.getByDoc(req.body.doc)) {
      res.status(400).send({
        message: "Documento já cadastrado!"
      });
      return;
    }
    if ( await repository.getByEmail(req.body.email)) {
      res.status(400).send({
        message: "Email já cadastrado!"
      });
      return;
    }
  } catch (e) {
    res.status(500).send({ 
      message: "Falha ao processar requisição!"
    });
  }

  repository.create(req.body)
    .then(data => {
      res.status(201).send({ 
        message: "Funcionário cadastrado com sucesso!" 
      });
    })
    .catch( e => {
      res.status(400).send({ 
        message: "Falha ao cadastrar Funcionário!", data: e 
      });
    });
};

exports.put = async (req, res, next) => {
  let id = req.params.id;
  try {
    let employee = await repository.getByEmail(req.body.email);
    if(employee && employee._id != id) {
      res.status(400).send({
        message: "Email já cadastrado!"
      });
      return;
    }

    employee = await repository.getByDoc(req.body.doc);
    if (employee && employee._id != id) {
      res.status(400).send({
        message: "Documento já cadastrado!"
      });
      return;
    }
  } catch (e) {
    res.status(500).send({ 
      message: "Falha ao processar requisição!"
    });
  }

  const errors = repository.validateForm(req.body)
  if (errors) {
    res.status(400).send(errors).end();
    return;
  }
  
  Employee.findByIdAndUpdate(id, {
    $set: {
      name: req.body.name,
      doc: req.body.doc,
      obs: req.body.obs,
      email: req.body.email,
      rank: req.body.rank,
      tags: req.body.tags
    }
  }).then(data => {
    res.status(200).send({
      message: 'Funcionário atualizado com sucesso!'
    });
  }).catch(e => {
    res.status(400).send({
      message: 'Falha ao atualizar funcionário',
      data: e
    });
  })
};

exports.delete = (req, res, next) => {
  let id = req.body.id;
  
  Employee.findByIdAndRemove(id)
  .then(data => {
    res.status(200).send({
      message: 'Funcionário removido com sucesso!'
    });
  }).catch(e => {
    res.status(400).send({
      message: 'Falha ao remover funcionário',
      data: e
    });
  })
};
