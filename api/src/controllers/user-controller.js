'use strict';

const Validation = require('../validators/validator');
const repository = require('../repositories/user-repository');
const auth = require('../repositories/auth-repository');
const authService = require('../services/auth-service');
const md5 = require('md5');

const emailService = require('../services/email-service');

exports.create = async (req, res, next) => {
  let validation = new Validation();
  validation.hasMinLen(req.body.name, 3, "O nome deve conter mais de 3 caracteres");
  validation.isRequired(req.body.email, "O email é obrigatório");
  validation.isLenSmallThan(req.body.password, 6, "A senha deve possuir mais de 6 caracteres");

  if(!validation.isValid()){
    res.status(400).send(validation.errors()).end();
    return;
  }

  try{
    await repository.create({
      name: req.body.name,
      email: req.body.email,
      password: md5(req.body.password + global.SALT_KEY)
    });

    emailService.send(
                      req.body.email, 
                      "Bem vindo ao teste da Lead2B", 
                      global.EMAIL_TMPL.replace('{0}', req.body.name));

    res.status(201).send({
      message: 'Cadastro realizado com successo, verifique sua caixa de email!'
    })
  } catch (e) {
    res.status(500).send({ 
      message: "Falha ao cadastrar Usuário!"
    });
  }
};

exports.get = async (req, res, next) => {
  try {
    let data = await repository.get();
    res.status(201).send(data);
  } catch (e) {
    res.status(500).send({ 
      message: "Falha ao processar requisição!"
    });
  }
}

exports.getbyId = async (req, res, next) => {
  try {
    let data = await repository.getById(req.params.id);
    res.status(201).send(data)
  } catch (e) {
    res.status(500).send({ 
      message: "Falha ao processar requisição!"
    });
  }
}

exports.put = async(req, res, next) => {
  try {
      await repository.update(req.params.id, req.body);
      res.status(200).send({
          message: 'Usuário atualizado com sucesso!'
      });
  } catch (e) {
      res.status(500).send({
          message: 'Falha ao processar requisição!'
      });
  }
};

exports.delete = async (req, res, next) => {
  try{
    await repository.delete(req.body.id);
    res.status(201).send({
      message: 'Usuário excluído com sucesso!'
    })
  } catch (e) {
    res.status(500).send({ 
      message: "Falha ao excluir Usuário!"
    });
  }
};

exports.authenticate = async (req, res, next) => {
  let validation = new Validation();
  validation.isRequired(req.body.email, "O email é obrigatório");

  if(!validation.isValid()){
    res.status(400).send(validation.errors()).end();
    return;
  }

  try{
    const user = await auth.authenticate({
      email: req.body.email,
      password: md5(req.body.password + global.SALT_KEY)
    });

    if(!user){
      res.status(404).send({
        message: "usuário ou senha inválida"
      });
      return;
    }

    const token = await authService.generateToken({
      id: user._id,
      email: user.email, 
      name: user.name
    });

    res.status(201).send({
      token: token,
      user: {
        id: user._id,
        email: user.email, 
        name: user.name
      }
    })
  } catch (e) {
    res.status(500).send({ 
      message: "Falha ao processar requisição!"
    });
  }
};

exports.refreshToken = async (req, res, next) => {
  try{
    const token = req.body.token || req.query.token || req.headers['authorization'];
    const data = await authService.decodeToken(token);
    
    const user = await user.getById(data.id);

    if(!user){
      res.status(401).send({
        message: "Usuário não encontrado"
      });
      return;
    }

    const tokenData = await authService.generateToken({
      id: user._id,
      email: user.email, 
      name: user.name
    });

    res.status(201).send({
      token: tokenData,
      data: {
        email: user.email, 
        name: user.name
      }
    })
  } catch (e) {
    res.status(500).send({ 
      message: "Falha ao processar requisição!"
    });
  }
};